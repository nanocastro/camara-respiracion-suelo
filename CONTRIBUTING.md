# CRS: Guía de Colaboración

## Reportes de problemas y pedidos de mejoras

Para reportar problemas con el equipo/software o solicitar alguna mejora
crea un [issue](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/issues). Pero antes de hacerlo revida lo siguiente:

* **Ya se ha reportado el problema**? Revisa la [lista de issues](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/issues).
* **Ya se ha solucionado**? Revisa que tu issue/mejora no haya sido ya resuelto/implementado.
* **Puedes arreglarlo tú**? Si eres un desarrollador con conocimientos de Arduino y/o Python, eres bienvenido a colaborar. Mira la sección [Contribuir con el código](#contribuir-con-el-código) para más información.
* **Está en un idioma que no es Español, Portugués o Inglés**? Para poder atender a tu pedido necesitamos que escribas el issue en uno de esos tres idiomas.
* **Sé bien específicx**: Mientras más detalles puedas dar sobre cómo reproducir un problema o qué funcionalidad quisieras que incorporáramos, más fácil
resulta responder a tu pedido!

## Contribuir con el código

* Si quieres ayudar con un problema existente o con una nueta mejora, **deja un comentario** en el **issue** correspondiente indicando que vas a trabajar en ello.
* Si no existe un **issue** para eso en lo que quieres contribuir, **crea uno nuevo** describiendo qué es lo que vas a hacer. Ésto le permite a la comunidad
hacerte algunas devoluciones antes que inviertas tiempo y energía en desarrollar algo que quizás deba hacerse de otra manera o que quizás ya se haya decidido que
no es una buena idea.
* Si eres **developer** del proyecto (puedes solicitar serlo comunicándote con nosotros), crea una **nueva rama** para tu desarrollo, que tenga un nombre apropiado
como *reloj_de_tiempo_real*. Éste estilo de trabajo se conoce como *feature branch workflow*. Al finalizar tu trabajo, puedes enviar un **merge request** para que
incluyamos tus aportes en la rama de distribución (**master**).
* Si no eres miembro del proyecto, puedes hacer un **fork** (copia) en tu cuenta personal y trabajar desde allí. Luego puedes enviar un **merge request** para que
incluyamos tus aprotes en el proyecto.
* Al finalizar tus aportes, no olvides comentar el issue correspondiente y cerrarlo si consideras que no queda nada por hacer.


