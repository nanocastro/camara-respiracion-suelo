[Enlish Version](README_EN.md)

# CRS - Cámara de respiración de suelo

![](/Imagenes/respira.jpeg)

## Qué es?

La cámara de respiración de suelo es un prototipo científico y educativo libre y de bajo costo para medir y visualizar el flujo de CO2 proveniente del suelo.
<details><summary> Mas info acerca de este prototipo</summary>
<p>
Este prototipo es parte del trabajo que estamos desarrollando en el [Laboratorio agroecológico abierto](https://regosh.libres.cc/proyectos/lab-agroeco-abierto/) con el nodo [ReGOSH de Mendoza](https://regosh.libres.cc/).
Colaboramos activamente [Hackteria](https://www.hackteria.org/wiki/UROS) y participamos de [Biomaker Challenge](https://www.biomaker.org/challenge) y [Open Hardware Makers 2022](https://openhardware.space/projects/).

El desarrollo está inspirado en el [prototipo abierto de Hackteria](https://www.hackteria.org/wiki/CO2_Soil_Respiration_Chamber).
</details>

## Por qué es importante la respiración del suelo?

La actividad microbiológica y el contenido de materia orgánica del suelo son indicadores clave de la salud del suelo. Los microorganismos al degradar la materia orgánica presente en el suelo generan un flujo de CO2 hacia la atmósfera llamado respiración.  Esta actividad está asociada al ciclo del carbono en el suelo y a la disponibilidad de nutrientes para las plantas que allí crecen.

## A quien está dirigida?

A campesinos, agricultores, estudiantes e investigadores que estudien la vida del suelo y quieran aprender acerca del rol de la respiración en el ciclo del carbono y los nutrientes.

## Construir una CRS

<img src=/Imagenes/WhatsApp%20Image%202021-08-02%20at%2021.34.24%20(1).jpeg alt="SRC" width="300"/> <img src=/Imagenes/screenshot.png  alt="SRC" width="400"/>

> [Guía de ensamblado](/Docs/Guia_ensamblado.md) 

Este prototipo se basa en un Arduino UNO y utiliza un sensor infrarrojo MH-Z19B en el interior de una cámara para medir el flujo de CO2 del suelo . Los datos son transmitidos por bluetooth y visualizados por medio de una [aplicación escrita en Python](https://gitlab.fcen.uncu.edu.ar/pcremades/co2-chamber-plot) que además permite almacenar los datos y  calcular la regresión lineal de un intervalo deseado. 
El dispositivo mide también la temperatura  y la humedad de suelo 

<details><summary> Prototipos experimentales</summary>
<p>
Actualmente estamos trabajando también en mejorar esta versión y estamos colaborando con Hackteria en versiones [experimentales de la CRS](https://www.hackteria.org/wiki/CO2_Soil_Respiration_Chamber)
</details> 

## Usar una CRS

> [Guía de instalación y uso de firmware y software](/Docs/Guia_uso_tecnica.md)

> [Guía de uso experimental](/Docs/Guia_uso_experimental.md) 

## Cómo participar del proyecto?
### Como usuario
Puedes dejarnos un [issue](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/issues) si has descubierto algún error o para sugerir nuevas funcionalidades. Ver [Colaborar con el desarrollo](#colaborar-con-el-desarrollo) 

Para contarnos de tu experiencia, para preguntas generales acerca respiración de suelo o si quieres conectarte con otras personas interesadas en el tema puedes escribir en el [Foro GOSH](https://forum.openhardware.science/c/projects/camara-de-respiracion-de-suelo/56)

### Colaborar con el desarrollo
Si te interesa participar con el desarrollo, puedes colaborar con nuevas ideas, traducciones, cambios en el diseño, mejoras en el código. La ayuda siempre es bienvenida. Mientras más somos, mejor es el proyecto!

Si quieres sumarte, mira las [notas de contribución](/CONTRIBUTING.md)

### Licencias
**Hardware**: Este proyecto es de código abierto y se libera bajo [licencia CERN v1.2](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/blob/master/License).  
**Documentación**: la documentación se libera bajo licencia Creative Commons Attribution Share Alike 4.0 International (CC-BY-SA-4.0)

