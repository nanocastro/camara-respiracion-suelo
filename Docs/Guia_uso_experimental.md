## Nociones básicas de respiración


[USDA Fact Sheet](https://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/nrcs142p2_053267.pdf)  

[USDA short video](https://www.youtube.com/watch?v=be51efbavTo) 

[W. Brinton talk](https://www.youtube.com/watch?v=XZzd-JWgPzQ&ab_channel=WoodsEndLaboratory)  

## Distintos protocolos de medición

[Aquí](/Referencias/Talking_SMAAC_A_New_Tool_to_Measure_Soil_Respirati.pdf) se puede encontrar una referencia de las distintas formas de medir con algunas indicaciones de los protocolos  
- En campo -> mediciónes de 2 minutos
  - [protocolo de LICOR](https://www.youtube.com/watch?v=1EkkB8JaIzQ&ab_channel=MethodsinSoilOrganicMatterandBiogeochemistry)   
- En laboratorio - 24h burst test
  - [protocolo de Solvita](https://www.youtube.com/watch?v=_x7i7pDybJI)
- Soil induced respiration

## Cómo interpretar los resultados?
- calculo de flujo para campo   
![co2 flux](/Docs/CO2%20soil%20flux.png)  

- DeltaPPM para campo

## Mediciones iniciales
[Algunas experiencias de uso](Mediciones_iniciales.md)

## Test de sensores

Hicimos una prueba con tres [sensores de CO2](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/blob/master/Docs/CO2%20sensor%20performance%20test.md) distintos:
- Winsen MH-Z16
- Winsen MH-Z19
- Sensirion SCD41


