## Instalacion del firmware
Usar la última versión del [firmware](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/tree/master/Firmware)

El firmware se carga por medio de la [IDE de Arduino](https://www.arduino.cc/en/software).

![](Docs/img/ArduinoIDE.gif)

### Librerias utilizadas

* Para el MH-Z19 se utiliza una [librería](https://github.com/WifWaf/MH-Z19) que permite medir por debajo de 400ppm.
* Las libreías de la pantalla (LiquidCristal_I2C) y del sensor de temperatura (DSB18B20) se pueden descargar directamente del gestor de librerías de la IDE.

## Software para visualización de datos en PC

[Aplicación de Python](https://gitlab.fcen.uncu.edu.ar/pcremades/co2-chamber-plot) para visualizar y analizar los datos en tiempo real desarrollada por Pablo Cremades.

Instrucciones reducidas (ver documentación completa en el repo):

1. Descargar el software ([link](https://gitlab.fcen.uncu.edu.ar/pcremades/co2-chamber-plot/-/archive/main/co2-chamber-plot-main.zip)) y descomprimir el zip. Alternativamente, se puede clonar con git, usando el comando: `git clone https://gitlab.fcen.uncu.edu.ar/pcremades/co2-chamber-plot.git`.
2. Luego abrir una terminal (cmd en Windows) y seguir estos pasos:

```bash
# Entrar en la carpeta del software
cd co2-chamber-plot
# Instalar todo en un virtual environment
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
# Ejecutar el software
python3 ArduinoPlot.py
```

[Soft alternativo](https://snapcraft.io/tauno-serial-plotter) para graficar el serial.

## Utilizar la cámara sin una PC

Puedes utilizar una aplicación para conectarte a la CRS con un dispositivo móvil y luego enviar los datos que
hayas tomado por mail o cualquier otro medio. Recomendamos utilizar la aplicación [Serial Bluetooth Terminal](https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal&gl=US)

1. Emparejar el dispositivo móvil con la CRS. Aparecerá en la lista de dispositivos Bluetooth como **HC-06**.
2. Una vez emaprejados los dispositivos, abrir la aplicación Serial Bluetooth Terminal y conectarse a la cámara.
3. Cuando haya terminado de tomar datos, desconectar la aplicación y guardar los datos.

![](Docs/img/BluetoothTerm.gif)
