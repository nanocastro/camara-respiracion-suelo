/*------------------------------------------------------------------------------
| Copyright Pablo Cremades 2022                                                |
|                                                                              |
| This source describes Open Hardware and is licensed under the CERN-OHL-S v2. |
|                                                                              |
| You may redistribute and modify this source and make products using it under |
| the terms of the CERN-OHL-S v2 (https://ohwr.org/cern_ohl_s_v2.txt).         |
|                                                                              |
| This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
| INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
| PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.  |
|                                                                              |
| Source location: https://example_url                                         |
|                                                                              |
| As per CERN-OHL-S v2 section 4, should You produce hardware based on this    |
| source, You must where practicable maintain the Source Location visible      |
| on the external case of the Gizmo or other products you make using this      |
| source.                                                                      |
 ------------------------------------------------------------------------------*/
/*
Description: this is just a rectangular platter with holes for the G12 conectors
(15.5mm in diameter) that can be glued to the electronics housing so that you
don't have to worry about making the holes nice and with the correct diameter.
Just make the holes bigger and separated 25mm from each other.

------------------------------
|                            |
|   /""\     /""\     /**\   |
|  |    |   |    |   |    |  |
|   \  /     \  /     \  /   |
|    **       **       **    |
------------------------------
*/

width = 80;
height = 30;
drillHole = 15.6;
holeSeparation = 25;

difference(){
  cube([width, height, 2]);
  holes();
}
module holes(){
  union(){
    for( i = [0:2] ){
      translate([holeSeparation*i + width/2 - holeSeparation, height/2, -1]) cylinder(d=drillHole, h=4);
    }
  }
}
