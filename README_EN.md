# SRC - Soil respiration chamber

![](/Imagenes/respira.jpeg)

## What is it ?

The SRC is a open source and low-cost scientific and educative prototype to measure and visualize the CO2 flux coming from the soil. 

<details><summary> More info about this prototype</summary>
<p>
This prototype is part of a larger effort of the [reGOSH Mendoza node](https://regosh.libres.cc/) to build and [Open Agroecology Lab](https://regosh.libres.cc/en/projects/open-agroecology-lab/).
We are working with [Hackteria network](https://www.hackteria.org/wiki/UROS) and the project is part of the [Biomaker Challenge 2022](https://www.biomaker.org/challenge) and [Open Hardware Makers 2022](https://openhardware.space/projects/).

The desing was inspired by the [open source prototype of Hackteria](https://www.hackteria.org/wiki/CO2_Soil_Respiration_Chamber).
</details>

## Why we want to measure soil respiration?

Microbiologycal activity and organic matter content are key indicators of soil health. The activity of microorganisms in soil, degrading organic matter, release a flux of CO2 to the atmosphere called respiration. This activity is related to the carbon cycle in the soil and to the  avaliability of nutrients for plants. 

## Who is this for?

We aimed at peasants, farmers, students and researchers that study and work with soil life and want to learn about the role of respiration in the carbon and nutrient cycling in soil.

## Building a SRC

<img src=/Imagenes/WhatsApp%20Image%202021-08-02%20at%2021.34.24%20(1).jpeg alt="SRC" width="300"/> <img src=/Imagenes/screenshot.png  alt="SRC" width="400"/>

> [Assembly guide](/Docs/Guia_ensamblado.md) 

This prototype is based on an Arduino UNO and uses a Winsen MH-Z19B IR sensor inside a chamber to measure the flux of CO2 coming from the soil. Data is transmitted via bluetooth to a [Python app](https://gitlab.fcen.uncu.edu.ar/pcremades/co2-chamber-plot) for real time visualization and  [aplicación escrita en Python]. The app allows to save the data and linear regression calculations.  . 
The device measures also soil temperature and humidity. 

<details><summary> Other prototypes</summary>
<p>
We are currently working on improving this version and on aI [IOT version](https://www.hackteria.org/wiki/CO2_Soil_Respiration_Chamber) in collaboration with  Hackteria. 
</details> 

## Using a SRC

> [Firmware and software installation and use guide](/Docs/Guia_uso_tecnica.md)

> [Experimental use guide](/Docs/Guia_uso_experimental.md) 

## How to get involved?
### As user
You can leave us an  [issue](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/issues) if you spot any bug or if you want to ask for new functionality . See [Collaborating with development](#colaborar-con-el-desarrollo) 

If you want to share your experience, ask general questions about soil respiration or if you want to connect with other peolple intereste in the subject you can write aht the [GOSH Forum](https://forum.openhardware.science/c/projects/camara-de-respiracion-de-suelo/56)

### Helping with development
If you want to get involved with the develpment of prototypes you can collaborate with new ideas, translations, design changes, code improvement. Help is always welcomed!

If you want to jump in, check the [contributing notes](/CONTRIBUTING.md)

### License
**Hardware**:  [CERN v1.2](https://gitlab.com/nanocastro/camara-respiracion-suelo/-/blob/master/License).  
**Documentation**: Creative Commons Attribution Share Alike 4.0 International (CC-BY-SA-4.0)

